package experiments;

import java.util.Random;

public class Cviko08
{
    public static void main(String[] args)
    {
        Cviko08_Heap heap = new Cviko08_Heap(10_000_000);
        Random random = new Random();
        int count = 8_000_000;
        long startTime = System.nanoTime();
        for(int i=0;i<count;i++)
        {
            double key = random.nextDouble()*100;
            Cviko05_Item payload = new Cviko05_Item(new double[]{0,0},"");
            heap.insert(key, payload);
        }
        long endTime = System.nanoTime();
        System.out.println(
                "Vkládání "+count+" položek trvalo "+ ( endTime - startTime )/1_000_000_000f + " sekund");

        long extractCount = 8_000;
        long startTime2 = System.nanoTime();
        for(int i=0;i<extractCount;i++)
        {
            //double min= heap.getMinKey();
            //System.out.println("min");
            heap.extractMin();
        }
        long endTime2 = System.nanoTime();
        System.out.println(
                "Odebrání "+extractCount+" položek trvalo "+ ( endTime2 - startTime2 )/1_000_000_000f + " sekund");
    }
}
