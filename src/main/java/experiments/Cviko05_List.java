package experiments;

import java.awt.event.ItemEvent;
import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class Cviko05_List implements Iterable<Cviko05_Item>
{
    private Cviko05_ListNode firstNode;
    private int nodeCount;

    public void addFirst(Cviko05_Item payload)
    {
       Cviko05_ListNode newNode = new Cviko05_ListNode(payload, firstNode);
       firstNode = newNode;
       nodeCount++;
    }

    public Cviko05_Item removeFirst()
    {
        if(nodeCount == 0)
        {
            throw new NoSuchElementException();
        }
        Cviko05_ListNode temp = firstNode;
        firstNode = firstNode.getNextNode();
        nodeCount--;
        return temp.getPayload();
    }

    public Cviko05_Item removeLast()
    {
        if(nodeCount == 0)
        {
            throw new NoSuchElementException();
        }
        Cviko05_ListNode node = firstNode;
        // Dojdi k předposlednímu uzlu
        for(int i=0; i<nodeCount-1; i++)
        {
            node = node.getNextNode();
        }

        Cviko05_ListNode lastNode = node.getNextNode();
        node.setNextNode(null);
        nodeCount--;
        return lastNode.getPayload();
    }

    public int size()
    {
        return nodeCount;
    }

    public Cviko05_Item get(int index)
    {
        Cviko05_ListNode currentNode = this.firstNode;
        for(int i=0; i<index; i++)
        {
            currentNode = currentNode.getNextNode();
            if(currentNode == null)
            {
                throw new IndexOutOfBoundsException();
            }
        }
        return  currentNode.getPayload();
    }

    public Iterator<Cviko05_Item> iterator()
    {
        return new Cviko05_ListIterator(firstNode);
    }
}
