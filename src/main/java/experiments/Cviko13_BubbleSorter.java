package experiments;

public class Cviko13_BubbleSorter extends Cviko09_Sorter
{
    public void sort(Comparable[] sequence)
    {
        for(int i=0; i<sequence.length; i++) // Celé probublání se opakuje
        {
            for(int j=0; j<sequence.length - 1 - i; j++) // Bublinka postupuje doprava
            {
                relax(sequence, j, j+1);
            }
        }
    }

    // i1 je niží index než i2
    private void relax(Comparable[] sequence, int i1, int i2)
    {
        boolean i1isLower= sequence[i1].compareTo(sequence[i2]) < 0;
        if(! i1isLower)
        {
            Comparable temp = sequence[i1];
            sequence[i1] = sequence[i2];
            sequence[i2] = temp;
        }
    }

}
