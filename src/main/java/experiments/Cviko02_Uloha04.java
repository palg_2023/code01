package experiments;

public class Cviko02_Uloha04
{
    public static void main(String[] args)
    {
    }

    // Doplň metodu, která informuje o tom, zda je daný rok přestupný
    public static boolean isLeapYear( int year )
    {
        if(year % 4 != 0)
        {
            return false;
        }
        if(year % 100 != 0)
        {
            return true;
        }
        if(year % 400 != 0)
        {
            return false;
        }
        return true;
    }
}
