package experiments;
import java.util.*;

public class Cviko07
{
    public static void main(String[] args)
    {
        Cviko07_Tree collection
                = new Cviko07_Tree();
        Random random = new Random();
        int count = 8_000_000;
        long startTime = System.nanoTime();
        long[] allKeys = new long[count];
        for(int i=0;i<count;i++)
        {
            long key = random.nextLong();
            allKeys[i] = key;
            Cviko05_Item value
                    = new Cviko05_Item(new double[]{0,0},"");
            collection.put(key, value);
        }
        long endTime = System.nanoTime();

        System.out.println(
                "Vkládání "+count+" položek trvalo "+ ( endTime - startTime )/1_000_000_000f + " sekund");

        long startTime1 = System.nanoTime();

        for(int j =0; j< 1_000_000; j++)
        {
            collection.get(allKeys[8*j]);
        }
        long endTime1 = System.nanoTime();
        System.out.println(
                "Hledání "+1_000_000+" položek trvalo "+ ( endTime1 - startTime1 )/1_000_000_000f + " sekund");

        long startTime2 = System.nanoTime();
        for(int j =0; j< 10_000; j++)
        {
            long findKey = allKeys[random.nextInt(count)];
            for (int s=0;s<allKeys.length;s++)
            {
                if(allKeys[s]==findKey)
                    break;
            }
            collection.get(allKeys[8*j]);
        }
        long endTime2 = System.nanoTime();
        System.out.println(
                "Hledání "+10_000+" položek V NESEŘAZENÉM POLI trvalo "+ ( endTime2 - startTime2 )/1_000_000_000f + " sekund");

    }
}
