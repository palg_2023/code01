package experiments;

import java.util.LinkedList;

public class Cviko05B_FifoCollection
        implements Cviko05B_InOutCollection
{
    Cviko05_List list = new Cviko05_List();

    public void in(Cviko05_Item item)
    {
        list.addFirst(item);
    }

    public Cviko05_Item out()
    {
        return list.removeLast();
    }
}
