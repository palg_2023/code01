package experiments;

import javax.rmi.ssl.SslRMIClientSocketFactory;

public class Cviko02_Uloha05
{
    public static void main(String[] args)
    {
        int zerosCount=maxZeroIntervalLength(new int[]{45,9,0,0,0,48,0,0,0,0,0});
        System.out.println(zerosCount);
    }

    // Doplň metodu, která informuje o tom, jaký nejvyšší počet nul se v poli vyskytuje za sebou
    public static int maxZeroIntervalLength( int[] hodnoty )
    {
        int current=0;
        int max=0;

        for(int i=0;i<hodnoty.length;i++)
        {
            if(hodnoty[i] == 0)
            {
                current++;
            }
            else
            {
                if(current > max)
                {
                    max = current;
                }
                current = 0;
            }
        }

        if(current > max)
        {
            max = current;
        }
        return max;
    }
}
