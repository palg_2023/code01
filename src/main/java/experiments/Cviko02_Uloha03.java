package experiments;

public class Cviko02_Uloha03
{
    public static void main(String[] args)
    {
    }

    // Doplň metodu, která informuje o tom, zda je dané pole seřazené vzestupně
    public static boolean isOrdered( double[] hodnoty )
    {
        for(int i=1; i<hodnoty.length;i++)
        {
            if(hodnoty[i-1] > hodnoty[i])
            {
                return false;
            }
        }
        return true;
    }
}
