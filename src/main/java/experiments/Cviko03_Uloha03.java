package experiments;

public class Cviko03_Uloha03
{
    public static void main(String[] args)
    {
    }

    // Doplň metodu, která spočítá součet všech prvků 2D pole
    public static double sum(double[][] matrix)
    {
        double sum = 0;
        int width = matrix[0].length;
        for(int i=0; i<matrix.length; i++)
        {
            for(int j=0; j<width; j++)
            {
                sum += matrix[i][j];
            }
        }

        return sum;
    }
}
