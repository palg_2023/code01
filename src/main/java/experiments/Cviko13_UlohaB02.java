package experiments;

import java.io.FileNotFoundException;
import java.time.Instant;

public class Cviko13_UlohaB02
{
    public static void main(String[] args) throws FileNotFoundException
    {
        // ÚLOHA 1
        for(int i =0; i< 10; i++);
        {
            System.out.println("AHOJ");
        }

        // ÚLOHA 2
        long now =Instant.now().getEpochSecond();
        if(now > 10_000_000_000L) System.out.println("OK 1");
        {
            System.out.println("OK 2");
        }

        // ÚLOHA 3
        int mid = 1000;
        System.out.println(mid + -5);

        // ÚLOHA 4
        int negative = Integer.MIN_VALUE;
        negative = Math.addExact(negative,-1);
        //negative--;
        System.out.println(negative);
    }
}
