package experiments;

import java.util.ArrayList;

public class Cviko02_Uloha02
{
    public static void main(String[] args)
    {
    }

    // Doplň metodu, která vrátí součet těch čísel v poli, která jsou nižší než daný limit
    public static int sumOfSmall( int[] hodnoty, int limit )
    {
        int sum = 0;
        for(int i=0; i <hodnoty.length; i++)
        {
            if(hodnoty[i] < limit)
            {
                sum += hodnoty[i];
            }
        }
        return sum;
    }
}
