package experiments;

import java.io.FileNotFoundException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Random;

public class Cviko13
{
    public static void main(String[] args) throws FileNotFoundException {
        Comparable[] example1 = new Instant[500];
        Comparable[] example2 = new LocalDateTime[500];
        Random r = new Random();
        for(int i =0; i< 500; i++)
        {
            long nowSeconds = Instant.now().getEpochSecond();
            long randomSeconds = r.nextInt(3600*24);
            example1[i]= Instant.ofEpochSecond(nowSeconds + randomSeconds);
        }

        Cviko09_Sorter s = new Cviko13_BubbleSorter();
        s.sortWithReport(example1);
    }
}
