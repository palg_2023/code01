package experiments;

public class Cviko08_Bonus
{
    public static void main(String[] args)
    {
        long n = 6468546646775L;
        PrintCollatzStepsV2(n);
    }

    // Doplň metodu, která bude opakovaně upravovat
    // zadané číslo N podle pravidla:
    //    pokud je N sudé, uprav na N/2
    //    pokud je N liché, uprav na N*3+1
    // Každá hodnota se vypíše na konzoli
    // Program končí, když hodnota klesne na 1
    public static void PrintCollatzStepsV1(long n)
    {
        while(n>1)
        {
            System.out.println(n);
            if(n%2==0)
            {
                n=n/2;
            }
            else
            {
                n=n*3+1;
            }
        }
    }

    public static void PrintCollatzStepsV2(long n)
    {
        if(n==1)
            return;
        System.out.println(n);
        if(n%2==0)
        {
            n=n/2;
        }
        else
        {
            n=n*3+1;
        }
        PrintCollatzStepsV2(n);
    }
}
